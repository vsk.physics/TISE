import numpy
from numpy import *
from scipy.integrate import simps


def second_derivative(data, h):
    out = numpy.array(data)
    for i in range(1, len(data) - 1):
        out[i] = data[i + 1] - 2 * data[i] + data[i - 1]
    out = out / (h * h)
    out[0] = out[1]
    out[-1] = out[-2]

    out[0] = (data[2] - 2 * data[1] + data[0])/(h**2)
    out[-1] = (data[-1] - 2 * data[-1-1] + data[-1-2])/(h**2)
    return out

    # return out


def heaviside(x):
    out = numpy.zeros_like(x)
    out[x >= 0] = 1.0
    return out


def get_Matrices(values_n, data_basis, data_potential, x):
    S = numpy.zeros(shape=(len(values_n), len(values_n)))
    T = numpy.zeros(shape=(len(values_n), len(values_n)))
    V = numpy.zeros(shape=(len(values_n), len(values_n)))
    for m, mval in enumerate(values_n):
        for n, nval in enumerate(values_n):
            S[m, n] = float("%0.4f" % simps(numpy.conjugate(
                data_basis[mval]) * data_basis[nval], x).real)
            T[m, n] = simps(numpy.conjugate(
                data_basis[mval]) * (-0.5) * second_derivative(
                data_basis[nval], x[1] - x[0]), x).real
            V[m, n] = simps(numpy.conjugate(
                data_basis[mval]) * data_potential * data_basis[nval], x).real
    return S, T, V


def eigen_vec_vals(S, values_n, ERROR, H):
    if numpy.allclose(S, numpy.identity(len(values_n)), atol=ERROR):
        E, C = numpy.linalg.eig(H)
        C_prime = C.copy()
    else:
        diag, U = numpy.linalg.eig(S)
        Sd = numpy.diag(diag)
        V = numpy.dot(U, numpy.linalg.inv(Sd)**0.5)

        H_prime = numpy.dot(numpy.dot(numpy.conjugate(V.T), H), V)
        E, C_prime = numpy.linalg.eig(H_prime)
        C = numpy.dot(V, C_prime)
    return E, C, C_prime
